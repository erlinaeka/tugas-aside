<x-guest.guestlayout title="Dinas Perumahan Rakyat, Kawasan Pemukiman dan Cipta Karya | Daftar Artikel"
    pagetitle="Daftar Artikel">
    <x-slot name="content">
        <aside class="w-full lg:w-3/12 xl:w-3/12 flex flex-col items-center px-3">
            <x-guest.asidecard asideName="KATEGORI ARTIKEL">
                <x-slot name="isiasidecard">
                    <?php
							$article_category = \App\Models\Article_category::all();
						?>
                    @if ($article_category->count())
                    <ul>
                        @foreach ($article_category as $category)
                        <li class="flex"><a href="#"
                                class="text-gray-900  text-sm font-semibold text-textpu py-2 block flex-1">{{
                                $category->name }}</a><span
                                class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">{{
                                $category->article->where('status','5')->count() }}</span>
                        </li>
                        @endforeach
                    </ul>
                    @else
                    <p data-aos="fade-right" class="text-textpu py-4 font-bold text-sm text-center">Belum Ada Kategori
                        Artikel</p>
                    @endif
                </x-slot>
            </x-guest.asidecard>
        </aside>
        <section class="w-full lg:w-6/12 xl:w-6/12 flex flex-col px-3 bg-white">
            <!-- Bagian Artikel Start -->
            <p data-aos="fade-right" class="text-textpu py-4 font-bold text-lg text-center">ARTIKEL</p>
            <hr data-aos="fade-right" class="bg-gray-200 -mt-2 mb-4" style="border-top: 3px solid #ffc928">
            @if ($article->count())
            @foreach( $article as $data )
            <article data-aos="fade-right" data-aos-delay="150"
                class="flex lg:flex-row xl:flex-row kecil:flex-col md:flex-col shadow my-4">
                <!-- Article Image -->
                <div class="mx-auto center my-auto p-2 item-center flex">
                    <img src="{{asset('storage/fileimage/'. $data->image)}}"
                        style="width: 300px; height: 170px; max-width: 300px; max-height: 170px;"">
                </div>
                <div class=" bg-white flex flex-col justify-start p-2">
                    <a href="#"
                        class="text-textpu text-xs font-bold uppercase pb-1.5">{{$data->article_category->name}}</a>
                    <a href="#" class="text-base font-bold hover:text-gray-700 pb-1">{{ $data->title }}</a>
                    <p class="text-xs pb-3">
                        By {{$data->user->name}} Published on {{$data->created_at->format('d F Y')}}
                    </p>
                    <h4 class="pb-3 text-xs text-justify">{!! Str::limit($data->content,500) !!}</h4>
                    <a href="{{ route('article-detail-guest', $data->slug) }}"
                        class="text-xs font-bold text-textpu hover:text-black">Continue Reading
                        <i class="fas fa-arrow-right text-textpu"></i></a>
                </div>
            </article>
            @endforeach
            @else
            <div class="bg-white flex flex-col justify-start p-2">
                <p class="pb-3 text-xs">No Results Found</p>
            </div>
            @endif

            {{-- PAGINASI NANTI DIGANTI --}}
            <span data-aos="fade-down" data-aos-delay="150"
                class="flex col-span-4 mt-7 xl:mt-7 sm:mt-auto sm:justify-start">
                {{ $article->links() }}
            </span>
            {{-- END PAGINASI  --}}
        </section>

        <aside class="w-full lg:w-3/12 xl:w-3/12 flex flex-col items-center px-3">
            <x-guest.tabaside titletab1="Kategori" titletab2="Terbaru">
                <x-slot name="cardtab1">
                    <ul>
                        <?php
							$article_category = \App\Models\Article_category::all();
						?>
                        @foreach($article_category as $category)
                        <li class="flex"><a href=""
                                class="text-textpu text-sm font-semibold text-textpu py-2 block flex-1">
                                {{$category->name}}</a>
                            <span
                                class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">{{ $category->article->count() }}</span>
                        </li>
                        @endforeach

                    </ul>
                </x-slot>
                <?php
				    $_article = \App\Models\Article::where('status', 5)->latest()->paginate(5);
				?>

                <x-slot name="cardtab2">
                    <ul>
                        @foreach( $_article as $_data )
                        <li class="mb-5">
                            <a href="{{ route('article-detail-guest', $_data->slug) }}">
                                <div>
                                    <h3 class="text-textpu text-sm font-semibold mb-1">{{$_data->title}}
                                    </h3>
                                    <span
                                        class="text-xs text-textpuor block mb-2">{{$_data->created_at->format('d F Y')}}</span>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </x-slot>
            </x-guest.tabaside>
        </aside>
    </x-slot>
</x-guest.guestlayout>