{{-- resources/views/livewire/Guest/home.blade.php --}}

<x-guest.guesthome title="Dinas Perumahan Rakyat, Kawasan Pemukiman dan Cipta Karya">
    {{-- Bagian Aside kiri start --}}
    <aside class="w-full lg:w-1/5 xl:w-1/5 flex flex-col items-center px-3">
        <x-guest.asidecard asideName="ASIDE KIRI">
            <x-slot name="isiasidecard">
                <ul class="text-textpu text-sm">
                    <li class="flex"><a href="#" class="text-gray-900  text-sm text-textpu py-2 block flex-1">Kategori
                            satu</a><span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">5</span>
                    </li>
                    <li class="flex"><a href="#" class="text-gray-900  text-sm text-textpu py-2 block flex-1">Kategori
                            dua</a><span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">12</span>
                    </li>
                    <li class="flex"><a href="#" class="text-gray-900  text-sm text-textpu py-2 block flex-1">Kategori
                            tiga</a><span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">1</span>
                    </li>
                    <li class="flex"><a href="#" class="text-gray-900  text-sm text-textpu py-2 block flex-1">Kategori
                            empat</a><span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">4</span>
                    </li>
                    <li class="flex"><a href="#" class="text-gray-900  text-sm text-textpu py-2 block flex-1">Kategori
                            lima</a><span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">10</span>
                    </li>
                </ul>
            </x-slot>
        </x-guest.asidecard>
    </aside>
    {{-- Bagian Aside kiri end --}}
    <section class="w-full lg:w-3/5 xl:w-3/5 flex flex-col px-3 bg-white">
        <!-- Bagian Artikel Start -->
        <p data-aos="fade-right" class="text-textpu py-4 font-bold text-lg text-center">ARTIKEL</p>
        <hr data-aos="fade-right" class="bg-gray-200 -mt-2 mb-4" style="border-top: 3px solid #ffc928">
        @if ($article->count())
        @foreach ($article as $article)
        <article data-aos="fade-right" data-aos-delay="150"
            class="flex lg:flex-row xl:flex-row kecil:flex-col md:flex-col shadow my-4">
            <!-- Article Image -->
            <div class="mx-auto center my-auto p-2 item-center flex">
                <img src="{{asset('storage/fileimage/'. $article->image)}}"
                    style="width: 300px; height: 170px; max-width: 300px; max-height: 170px;">
            </div>
            <div class="bg-white flex flex-col justify-start p-2">
                <a href="#" class="text-textpu text-xs font-bold uppercase pb-1.5">{{ $article->article_category->name
                    }}</a>
                <a href="/article-guest/{{ $article->slug }}"
                    class="text-base font-bold opacity-90 pb-1">{{$article->title}}</a>
                <p class="text-xs pb-3 text-textpu">
                    By {{ $article->user->name }} on {{ $article->created_at->format('d F Y') }}
                </p>
                <h4 class="pb-3 text-xs text-justify">{!! Str::limit($article->content,440) !!}</h4>

                <a href="/article-guest/{{ $article->slug }}" class="text-xs font-bold text-textpu hover:text-black"
                    style="display: inline-block;">Continue Reading <i class="fas fa-arrow-right text-textpu"></i></a>
            </div>
        </article>
        @endforeach
        @else
        <p data-aos="fade-right" class="text-textpu py-4 font-bold text-md text-center">Belum Ada Arikel Yang Dipublish
        </p>
        @endif


        <a data-aos="fade-right" href="/article-guest"
            class="bg-white py-2 w-56 mb-6 text-base text-textpu font-bold rounded-md">Artikel Lainnya <i
                class="fas fa-arrow-right text-textpu"></i></a>


        <!-- Bagian Social Media Start -->
        @include('components.guest.mediasocialsection')
        <!-- Bagian Social Media End -->
    </section>

    <!-- BAGIAN ASIDE KANAN START -->
    <aside class="w-full lg:w-1/5 xl:w-1/5 flex flex-col items-center px-3">
        <x-guest.asidecard asideName="KATEGORI ARTIKEL">
            <x-slot name="isiasidecard">
                @if ($article_category->count())
                <ul>
                    @foreach ($article_category as $category)
                    <li class="flex"><a href="#"
                            class="text-gray-900  text-sm font-semibold text-textpu py-2 block flex-1">{{
                            $category->name }}</a><span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">{{
                            $category->article->where('status','5')->count() }}</span>
                    </li>
                    @endforeach
                </ul>
                @else
                <p data-aos="fade-right" class="text-textpu py-4 font-bold text-sm text-center">Belum Ada Kategori
                    Artikel</p>
                @endif
            </x-slot>
        </x-guest.asidecard>

        <x-guest.asidecard asideName="DOKUMEN">
            <x-slot name="isiasidecard">
                @if ($documents->count())
                <ul>
                    @foreach ($documents as $doc)
                    <li class="mb-5">
                        <a href="#">
                            <div>
                                <h3 class="text-textpu text-sm font-semibold mb-1">{{ $doc->document_title }}</h3>
                                <span class="text-xs text-textpuor block mb-2">{{ $doc->created_at->format('d F Y')
                                    }}</span>
                            </div>
                        </a>
                    </li>
                    @endforeach
                </ul>
                <a href="/document-guest"
                    class="bg-transparent py-2 w-32 mb-2 mt-4 text-xs font-bold text-textpu rounded-md">Selengkapnya
                    <i class="fas fa-arrow-right text-textpu"></i></a>
                @else

                <p data-aos="fade-right" class="text-textpu py-4 font-bold text-sm text-center">Belum Ada Dokumen
                    Yang Dipublish</p>
                @endif
            </x-slot>
        </x-guest.asidecard>

        <x-guest.asidecard asideName="ALBUM">
            <x-slot name="isiasidecard">
                @if ($albums->count())
                <ul>
                    @foreach ($albums as $album)
                    <li class="flex"><a href="gallery-album-guest/{{ $album->id }}"
                            class="text-gray-900  text-sm font-semibold text-textpu py-2 block flex-1">{{
                            $album->album_title }}</a><a href="gallery-album-guest/{{ $album->id }}"
                            class="text-gray-700 text-sm font-semibold text-textpu  p-2  "><i
                                class="fas fa-arrow-right text-textpuor"></i></a></li>
                    @endforeach
                </ul>
                <li class="flex"><a href="/gallery-guest"
                        class="text-gray-900  text-sm font-semibold text-textpu py-2 block flex-1">Album Lainnya</a><a
                        href="/gallery-guest" class="text-gray-700 text-sm font-semibold text-textpu  p-2  "><i
                            class="fas fa-arrow-right text-textpuor"></i></a></li>
                @else
                <p data-aos="fade-right" class="text-textpu py-4 font-bold text-sm text-center">Belum Ada Album
                    Yang Dipublish</p>
                @endif

            </x-slot>
        </x-guest.asidecard>
    </aside>
    <!-- BAGIAN ASIDE KANAN END -->
    </div>
    <!-- End Bagian Isi Content -->

</x-guest.guesthome>