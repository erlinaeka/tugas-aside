{{-- resources/views/livewire/Guest/detail_artikel.blade.php --}}


<x-guest.guestlayout title="Dinas Perumahan Rakyat, Kawasan Pemukiman dan Cipta Karya | Detail Artikel"
    pagetitle="Daftar Artikel / Detail Artikel">
    <x-slot name="content">
        <aside class="w-full lg:w-3/12 xl:w-3/12 flex flex-col items-center px-3">
            <x-guest.asidecard asideName="KATEGORI ARTIKEL">
                <x-slot name="isiasidecard">
                    <?php
							$article_category = \App\Models\Article_category::all();
						?>
                    @if ($article_category->count())
                    <ul>
                        @foreach ($article_category as $category)
                        <li class="flex"><a href="#"
                                class="text-gray-900  text-sm font-semibold text-textpu py-2 block flex-1">{{
                                $category->name }}</a><span
                                class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">{{
                                $category->article->where('status','5')->count() }}</span>
                        </li>
                        @endforeach
                    </ul>
                    @else
                    <p data-aos="fade-right" class="text-textpu py-4 font-bold text-sm text-center">Belum Ada Kategori
                        Artikel</p>
                    @endif
                </x-slot>
            </x-guest.asidecard>
        </aside>
        @foreach( $article as $data )
        <section class="w-full lg:w-6/12 xl:w-6/12 px-3 bg-white">
            <!-- Bagian Artikel Start -->
            <p data-aos="fade-right" class="text-textpu py-4 font-bold text-lg text-center">{{ $data->title }}</p>
            <hr data-aos="fade-right" class="bg-gray-200 -mt-2 mb-4" style="border-top: 3px solid #ffc928">

            <article data-aos="fade-right" data-aos-delay="150"
                class="flex lg:flex-col xl:flex-col kecil:flex-col md:flex-col gap-4 shadow my-4">
                <!-- Article Image -->
                <div class="mx-auto center my-auto p-2 item-center flex">
                    <img src="{{asset('storage/fileimage/'. $data->image)}}" style="width: 650px; height: 270px;">
                </div>
                <div class="bg-white flex flex-col justify-start p-6 pt-4">
                    <p class="text-textpu text-xs font-bold uppercase pb-1.5">{{ $data->article_category->name }}</p>
                    <p class="text-xs pb-3  text-textpu">
                        By {{ $data->user->name }}, Published on {{ $data->created_at }}
                    </p>
                    <h4 class="pb-3 text-sm text-gray-600 text-justify">{!! $data->content
                        !!}</h4>
                    <p class="text-textpu text-xs font-bold pb-1.5 mt-4">Bagikan Artikel :</p>
                    <div class="social-btn-sp">
                        {!! Share::page(url('/article-guest/'. $data->slug))->facebook()->twitter() !!}
                    </div>

                </div>
            </article>
            <style>
                #social-links ul {
                    padding-left: 0;
                }

                #social-links ul li {
                    display: inline-block;
                }

                #social-links ul li a {
                    padding: 6px;
                    border: 1px solid #ccc;
                    border-radius: 5px;
                    margin: 1px;
                    font-size: 25px;
                }

                #social-links .fa-facebook {
                    color: #0d6efd;
                }

                #social-links .fa-twitter {
                    color: deepskyblue;
                }

                #social-links .fa-linkedin {
                    color: #0e76a8;
                }

                #social-links .fa-whatsapp {
                    color: #25D366
                }

                #social-links .fa-reddit {
                    color: #FF4500;
                    ;
                }

                #social-links .fa-telegram {
                    color: #0088cc;
                }
            </style>
            <a data-aos="fade-right" href="/article-guest"
                class="bg-white py-2 w-56 mb-6 text-sm text-textpu font-bold rounded-md">Daftar Artikel <i
                    class="fas fa-arrow-right text-textpu"></i></a>
            <!-- Bagian Artikel End -->

        </section>
        @endforeach
        <aside class="w-full lg:w-3/12 xl:w-3/12 flex flex-col items-center px-3">
            <x-guest.tabaside titletab1="Kategori" titletab2="Terbaru">
                <x-slot name="cardtab1">
                    <ul>
                        <?php
							$article_category = \App\Models\Article_category::all();
						?>
                        @foreach($article_category as $category)
                        <li class="flex"><a href=""
                                class="text-gray-900  text-sm font-semibold text-textpu py-2 block flex-1">
                                {{$category->name}}</a>
                            <span class="text-gray-700 text-sm font-semibold text-textpu  p-2 ">{{
                                $category->article->count() }}</span>
                        </li>
                        @endforeach
                    </ul>
                </x-slot>
                <?php
				    $_article = \App\Models\Article::where('status', 5)->latest()->paginate(5);
				?>

                <x-slot name="cardtab2">
                    <ul>
                        @foreach( $_article as $_data )
                        <li class="mb-5">
                            <a href="{{ route('article-detail-guest', $_data->slug) }}">
                                <div>
                                    <h3 class="text-textpu text-sm font-semibold mb-1">{{$_data->title}}
                                    </h3>
                                    <span class="text-xs text-textpuor block mb-2">{{$_data->created_at}}</span>
                                </div>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </x-slot>
            </x-guest.tabaside>
        </aside>
    </x-slot>
</x-guest.guestlayout>